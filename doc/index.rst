===================================================================
LaTeX template for Doctoral dissertation at Université Paris Saclay
===================================================================

    :Author: Dr. Óscar Nájera

This is a style package that can be used to generate the cover and abstract
pages of the Doctoral dissertation for the university of Paris-Saclay.

- The online documentation is here `https://titan-c.gitlab.io/latex_UPS_thesis/ <https://titan-c.gitlab.io/latex_UPS_thesis/>`_

- The source code repository is here `https://gitlab.com/Titan-C/latex_UPS_thesis/ <https://gitlab.com/Titan-C/latex_UPS_thesis/>`_

1 Installation
--------------

1.1 Git
~~~~~~~

You can download the entire repository with

.. code:: sh

    git clone https://gitlab.com/Titan-C/latex_UPS_thesis.git

2 Use
-----

You need to have the *upsfront.sty* file and the logos from your host
institution and the doctoral school. You can find those in the *logo*
folder. Place this files next to your latex file containing the
dissertation. Alternatively declare their location in your latex
configuration files.

3 The configuration
-------------------

This package is mostly independent of the latex class you use. I tested
with very few but I mostly liked the `SPhdThesis <https://ctan.org/pkg/sphdthesis>`_ class. The only requirement
in that the used class has the abstract environment which is modified
here [1]_ .

To use this package you need to put into your preamble the
``\usepackage{upsfront}`` and setup the variables as is shown next.

.. code:: tex

    \documentclass[a4paper,12pt]{SPhdThesis}
    \usepackage[utf8]{inputenc}
    \usepackage{upsfront} % This is our package
    \usepackage{lipsum} % Not necessary just to fill content
    \graphicspath{{./}} % declare where the package graphics are
    \NNT{à attribuer}
    \logoinstitution{logo/UPS.png} % Logo of Institution
    \logoED{logo/PIF.png} % Logo of Ecole Doctorale
    \ecodocnum{000}
    \ecodoctitle{Nom École doctorale}
    \PhDworkingplace{Université Paris-Sud}
    \PhDspeciality{\LaTeX} % Speciality
    \defenseplace{Orsay} %Place of defense
    \author{Your Name}
    \date{date of defense}
    \title{Title of your PhD}

In the *document* environment start with the *frontmatter* environment(for
SPhdthesis) to having roman numbers and other content at the
beginning. Some \LaTeX classes use a command instead of an environment be
careful how you override them. Then frontcover environment is from this
package and contains the regulated cover page. It takes the list of members
of the jury. Use a table as is most convenient for alignment

.. code:: tex

    \begin{document}
    \begin{frontmatter}

    \begin{frontcover}
    \begin{center}
    \begin{tabular}{llll}
    M. & \textsc{Name Surname} & Université Paris-Sud & (Directeur de thèse)\\
    M. & \textsc{Name Surname} & Université Paris-Sud & (Rapporteur)\\
    M. & \textsc{Name Surname} & Université Paris-Sud & (Membre du Jury)\\
    \end{tabular}
    \end{center}
    \end{frontcover}

The abstract environment, generates the isolated abstract pages for the
summaries. You are responsible for any extra heading you want inside of
those boxes. Follow the example bellow.

.. code:: tex

    \begin{abstract}
    \paragraph*{Title : Title of the PhD}
    \paragraph*{Keywords : } \LaTeX

    This package generates the cover page and the abstracts pages. It is only
    aimed at the cover style and not at the complete thesis styling.
    \end{abstract}

Finally, close with the table of contents and close the frontmatter
environment. Then you can continue writing your dissertation.

.. code:: tex

    \setcounter{tocdepth}{2}
    \tableofcontents
    \end{frontmatter}
    \chapter{First}
    \lipsum
    \section{First child}
    \lipsum
    \chapter{Second}
    \lipsum
    \end{document}

4 Sample documents
------------------

4.1 Starter doc
~~~~~~~~~~~~~~~

This package includes the latex-sample.tex file which is used to generated
this document.

- :download:`latex source example <../latex-sample.tex>`
- :download:`final pdf file <../latex-sample.pdf>`

.. raw:: html

    <object data="_downloads/latex-sample.pdf" type="application/pdf" width="500px" height="500px">
    <p>Alternative text - include a link <a href="_downloads/latex-sample.pdf">to the PDF!</a></p>
    </object>

4.2 Theses using this package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The package author own thesis
    `https://gitlab.com/Titan-C/dissertation <https://gitlab.com/Titan-C/dissertation>`_


.. [1] If your class does not have such environment you’ll have to edit the *upsfront.sty* file.
